package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    int max_size = 20;
    ArrayList<FridgeItem> ItemsInFridge = new ArrayList<FridgeItem>();

    
    @Override
    public int totalSize(){
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        // number of itmes in the fridge
        return ItemsInFridge.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // Place a food item in the fridge.
        if (totalSize() > nItemsInFridge()){
            ItemsInFridge.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // Remove item from fridge
        if (ItemsInFridge.contains(item)){
            ItemsInFridge.remove(item);
        } else{
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        // Remove all items from the fridge
        ItemsInFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // Remove all items that have expired from the fridge
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();

        for(FridgeItem i : ItemsInFridge){
            if (i.hasExpired()){
                expiredItems.add(i);
                //ItemsInFridge.remove(i);
            }
        }
        for (FridgeItem i : expiredItems){
            ItemsInFridge.remove(i);
        }
        return expiredItems;
    }
    
}
